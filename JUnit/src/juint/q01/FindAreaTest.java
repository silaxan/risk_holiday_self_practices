package juint.q01;
import org.junit.Test;

import junit.framework.Assert;

public class FindAreaTest {

	FindArea demo = new FindArea();
	
	@Test
	public void squre_Test() {
		Assert.assertEquals(25.0, demo.squre(5));
	}
	
	@Test
	public void triangle_Test() {
		Assert.assertEquals(24.0, demo.triangle(6.0 , 8.0));
	}
	
	@Test
	public void circle_Test() {
		Assert.assertEquals(615.7521601035994, demo.circle(14.0));
	}
	
	@Test
	public void recangle_Test() {
		Assert.assertEquals(50.0, demo.recangle(5.0 ,5.0));
	}
	
	
}
