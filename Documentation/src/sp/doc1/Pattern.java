package sp.doc1;
/**
 *  This is small pattern
 *  
 * @author Silaxan
 * @since 17-05-2020
 * @version 1.0.0
 *
 */

public class Pattern {
	/**
	 * 
	 * @param row  maximum length of this pattern-No 9
	 * @param num minimum length of this pattern-O1
	 */
	
	public static void pattern(int row , int num) {
	for(int i=1; i<=row; i++) {
		for(int j=row; j>=num; j--) {
			System.out.print(j);
		}
		System.out.println();
	
		num=num+1;
	    row=row-1;
	}
	}
	/**
	 * 
	 * @param args for input arguments 
	 */
    public static void main(String args[]) {
    	Pattern scan =new Pattern();
    	int row = 9;
   	 	int num = 1;
   	 	scan.pattern(row, num);
	}
}
