package sp.doc2;
/**
 * This pattern increament by 2 between 1-9
 * 
 * @author Silaxan
 * @since 18.05.2020
 * @version 1.0.0
 */
public class Pattern2 {
	/**
	 * 
	 * @param row maximum length of pattern value
	 * @param num minimum length of pattern value
	 */

	public static void DoubleForLoop(int row , int num) {
		
		for(int i=1; i<=row;i++) {
			for(int j=num; j<row; j++) {
				
			System.out.print(j);
			}
			System.out.println();
			num = num+2;
		}	
	}
		/**
		 *  
		 * @param args input arguments
		 */
	public static void main(String args []) {
		Pattern2 demo = new Pattern2();
		int row = 10;
		int num = 1;
		
		 demo.DoubleForLoop(row , num);	
	}
	
}
