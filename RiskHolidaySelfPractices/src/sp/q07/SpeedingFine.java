package sp.q07;
public class SpeedingFine {
	
	public static void SpeedingFine(int speed,int limit) {
		if(speed<=limit) {
			System.out.println("no fine");
		}else {
			int overspeed = speed - limit;
			int fine = 20*overspeed;
			System.out.println( "Question No 07 Speeding fine");
			System.out.println();
			System.out.println("The fine for driving at " + speed + "mph on a " +limit + "mph road is " + fine + " dollars.");
			System.out.println();
			
		}
	}
}

