package sp.q03;
	public class XwithXsDemo {
		public static void main(String args []) {
			XwithXs demo = new XwithXs();
			int x = 9;
			int row = 5;
			
			demo.shapex(x);
		}
	}
/*
	x       x
	 x     x 
	  x   x  
	   x x   
	    x    
	   x x   
	  x   x  
	 x     x 
	x       x
*/