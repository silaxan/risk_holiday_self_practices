package sp.q23;
public class StringMethod {
	public static void length(String word) {
		
		int length = word.length();
		System.out.println("length value : " + length);
		}
	
	public static void substring(String word) {
		
		String substring = word.substring(22);
		System.out.println("substring value : " + substring);
		}
	
	public static void substring1(String word) {
		
		String substring = word.substring(22, 24);
		System.out.println("substring value : " + substring);		
		}
	
	public static void indexOf(String word) {
		
		int index = word.indexOf("oo");
		System.out.println("index value     : " + index);
	}
	
	public static void toUpperCase(String word) {
		
		String UpperCase = word.toUpperCase();
		System.out.println("UpperCase value : " + UpperCase);
		}
	
	public static void lastIndexOf(String word) {
		
		int lastIndex = word.lastIndexOf("o");
		System.out.println("lastIndex value : " + lastIndex);
		}
	
	public static void indexOf1(String word) {
		
		int index = word.indexOf("ok");
		System.out.println("index value     : " + index);
		}
	
}
