package sp.q23;
public class StringMethodDemo {
	public static void main(String args[]) {
		StringMethod demo = new StringMethod();
			String word =  "School.of.Progressive. Rock";
			
			demo.length(word);
			demo.substring(word);
			demo.substring1(word);
			demo.indexOf(word);
			demo.toUpperCase(word);
			demo.lastIndexOf(word);
			demo.indexOf1(word);
	}
}

/*
length value : 27
substring value :  Rock
substring value :  R
index value     : 3
UpperCase value : SCHOOL.OF.PROGRESSIVE. ROCK
lastIndex value : 24
index value     : -1
*/