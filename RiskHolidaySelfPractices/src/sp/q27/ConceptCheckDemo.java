package sp.q27;
public class ConceptCheckDemo {
	public static void main(String args[]) {
		ConceptCheck demo = new ConceptCheck();
			String s = "Mississippi";
			
			demo.length(s);
			demo.indexOf(s);
			demo.toUpperCase(s);
			demo.toLowerCase(s);
			demo.substring(s);
			demo.lastIndexOf(s);	
	}
}
/*
length value                  : 11
index value                   : 3
toUpperCase indexOf value     : -1
toLowerCase indexOf value     : 3
substring indexOf value       : M
substring last Index Of value : i
*/