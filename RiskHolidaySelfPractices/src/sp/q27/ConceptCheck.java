package sp.q27;
public class ConceptCheck {
	public static void length(String s) {
		
		int length = s.length();
		System.out.println("length value                  : " + length);
		}
	
	public static void indexOf(String s) {
		
		int index = s.indexOf( "si" );
		System.out.println("index value                   : " + index);
		}
	
	public static void toUpperCase(String s) {
		
		int str = s.toUpperCase().indexOf( "si" );
		System.out.println("toUpperCase indexOf value     : " + str);		
		}
	
	public static void toLowerCase(String s) {
		
		int index = s.toLowerCase().indexOf( "si" );
		System.out.println("toLowerCase indexOf value     : " + index);
	}
	
	public static void substring(String s) {
		
		String UpperCase = s.substring(0,s.indexOf( "i" ));
		System.out.println("substring indexOf value       : " + UpperCase);
		}
	
	public static void lastIndexOf(String s) {
		
		String lastIndex = s.substring( s.lastIndexOf( "i" ));
		System.out.println("substring last Index Of value : " + lastIndex);
		}
	
}

