package sp.q01;
public class Diamond {
	
	public static void Diamond(int row) {
		
		for(int i=1; i<=row; i++ ) {
		
			for(int j=1; j<=row-i; j++) {
				System.out.print(" ");
				}
			System.out.print("/");
				
			for(int k=1; k<=(i*2)-2; k++) {
				System.out.print(" ");
			}	
			System.out.println("\\");
		}
		
		for(int i=1; i<=row; i++ ) {
			
			for(int j=1; j<=i-1; j++) {
				System.out.print(" ");
				}
			System.out.print("\\");
				
			for(int k=1; k<=(row-i)*2; k++) {
				System.out.print(" ");
			}	
			System.out.println("/");
		}
	
	}
}
