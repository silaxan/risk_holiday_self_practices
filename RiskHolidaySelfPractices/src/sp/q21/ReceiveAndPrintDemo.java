package sp.q21;
import java.util.Scanner;
public class ReceiveAndPrintDemo {
	public static void main(String args[]) {
		ReceiveAndPrint demo = new ReceiveAndPrint();
		Scanner scan =new Scanner(System.in);
		System.out.println("Enter the String Value : ");
		String s = scan.nextLine();
		System.out.println("Enter the integer Value : ");
		int m = scan.nextInt();
		System.out.println("Enter the Double Value : ");
		double d = scan.nextDouble();
		
		demo.ReceiveAndPrint(s, m, d);
	}
}

/*
------User view--------

Enter the String Value : 
Silaxan
Enter the integer Value : 
135
Enter the Double Value : 
135.5

------Output--------

Silaxan
135
135.5
270.5 Silaxan
270.5 Silaxan
135 Silaxan 135.5
Silaxan 135 135.5

*/