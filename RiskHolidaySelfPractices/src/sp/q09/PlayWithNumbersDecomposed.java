package sp.q09;
	public class PlayWithNumbersDecomposed {

		public static void NumberManipulation(int a, int b) {
			if( b>0) {
			int x= a + b;
			int y= a - b;
			int m= a * b;
			int d= a / b;
			int mod=a % b;
			System.out.println("------------------------------");
			System.out.println( "a + b is equal to " +x);
			System.out.println( "a - b is equal to " +y);
			System.out.println( "a * b is equal to " +m);
			System.out.println( "a / b is equal to " +d);
			System.out.println( "a % b is equal to " +mod);
			System.out.println();
			}else
				System.out.println( "Your Second number is invalid.");
			
		}
		
		
		public static void Manipulation(int x, int y, int z) {
			int num1 = (x - y)/z;
			int num2 = (x - z)/y;
			int num3 = (y - z)/x;
			int num4 = (y - x)/z;
			int num5 = (z - x)/y;
			int num6 = (z - y)/x;
			
			System.out.println("---------------------------------");
			System.out.println( "(x - y)/z is equal to " +num1);
			System.out.println( "(x - z)/y is equal to " +num2);
			System.out.println( "(y - z)/x is equal to " +num3);
			System.out.println( "(y - x)/z is equal to " +num4);
			System.out.println( "(z - x)/y is equal to " +num5);
			System.out.println( "(z - y)/x is equal to " +num6);
		
		}
		
		
		
	}
