package sp.q09;
import java.util.Scanner;
public class PlayWithNumbersDecomposedDemo{
	public static void main(String args []) {
		PlayWithNumbersDecomposed demo = new PlayWithNumbersDecomposed();
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the Number 1: ");
		int a = sc.nextInt();
		System.out.print("Enter the Number 2: ");
		int b = sc.nextInt();
		
		demo.NumberManipulation(a,b);
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter the Number 1: ");
		int x = sc.nextInt();
		System.out.print("Enter the Number 2: ");
		int y = sc.nextInt();
		System.out.print("Enter the Number 3: ");
		int z = scan.nextInt();
		
		
		
		
		demo.Manipulation(x,y,z);
	
	}
}

/*
Enter the Number 1: 5
Enter the Number 2: 13
------------------------------
a + b is equal to 18
a - b is equal to -8
a * b is equal to 65
a / b is equal to 0
a % b is equal to 5

Enter the Number 1: 5
Enter the Number 2: 13
Enter the Number 3: 5
---------------------------------
(x - y)/z is equal to -1
(x - z)/y is equal to 0
(y - z)/x is equal to 1
(y - x)/z is equal to 1
(z - x)/y is equal to 0
(z - y)/x is equal to -1

*/