package sp.q28;
public class StringReverse {
	public static void StringReverse(String name) {
		String reverse = "";
		
		for(int i=name.length()-1; i>=0; i--) {
			reverse = reverse + name.charAt(i);
		}
		System.out.println("The reverse of Computer-Programming is " + reverse);
	}
}
