package sp.q31;
import java.util.Scanner;
public class ToDecimalDemo {
		public static void main(String args[]) {
			ToDecimal demo = new ToDecimal();
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter The Binary Number : ");
			String binary = scan.nextLine();
			
			demo.ToDecimal(binary);
		}

}

/*
------user Input-------
Enter The Binary Number : 
11111011111


------Output---------
2015

*/