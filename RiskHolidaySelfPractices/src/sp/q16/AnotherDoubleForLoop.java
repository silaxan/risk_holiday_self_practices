package sp.q16;
public class AnotherDoubleForLoop {
	public static void AnotherDoubleForLoop(int row) {
		int num = 1;
		
		for(int i=1; i<=row; i++) {
			for(int j=row; j>=num; j--) {
				System.out.print(j);
			}
			System.out.println();
			num=num+1;
		    row=row-1;
		}
		
	}
}
