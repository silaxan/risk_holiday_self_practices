package sp.q24;
public class StringMethod {
	public static void indexOf(String w ,String pat ) {
		
		int index = w.indexOf(pat);
		System.out.println("index value     : " + index);
		}
	
	public static void indexOf1(String w ,String pat) {
		
		int index = w.indexOf(pat,3);
		System.out.println("index value     : " + index);
		}
	
	public static void indexOf2(String w ,String pat) {
		
		int index = w.indexOf(pat,6);
		System.out.println("index value     : " + index);		
		}
	
	public static void lastIndexOf(String w ,String pat) {
		
		int lastIndex = w.lastIndexOf(pat);
		System.out.println("lastIndex value : " + lastIndex);
	}
	
	public static void length(String w ,String pat) {
		
		int length = w.length();
		System.out.println("length value    : " + length);
		}
	
	public static void toUpperCase(String w ,String pat) {
		
		String UpperCase = w.toUpperCase();
		System.out.println("UpperCase value : " + UpperCase);
		}
	
	public static void charAt(String w ,String pat) {
		
		int charAt = w.charAt(0);
		System.out.println("char value      : " + charAt);
		}
	
}
