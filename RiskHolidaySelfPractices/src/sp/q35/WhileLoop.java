package sp.q35;
public class WhileLoop {
	public static void WhileLoop(int num) {
		while(num>=1) {
			int num1 = num/2;
			String num2 = String.format("%04d",num);
			System.out.println("+" + num2+ "?");
			num=num1;
		}
	}
}
