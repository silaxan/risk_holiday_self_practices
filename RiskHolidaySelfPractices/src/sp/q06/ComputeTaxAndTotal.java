package sp.q06;
public class ComputeTaxAndTotal {
	public static void TaxAndTotal(int subtotal, double taxrate) {
	
	int tax = (int) ((subtotal * taxrate)/100);
	int total = (int) (subtotal + tax);
	
	int dollers1 = subtotal/100;
	int cents1 = subtotal%100;
	int dollers2 = tax / 100;
	int cents2 = tax % 100;
	int dollers3 = total/100;
	int cents3 = total%100;
	

		System.out.println("The subtotal = " + dollers1 + " dollars and " + cents1 + " cents");
		System.out.println("The tax rate = " + taxrate + " percent.");
		System.out.println("The tax = " + dollers2 + " dollars and " + cents2 + " cents");
		System.out.println("The total = " + dollers3 + " dollars and " + cents3 + " cents");

			
	}
}
