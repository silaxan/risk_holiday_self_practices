package sp.q06;
public class ComputeTaxAndTotalDemo {
	public static void main(String args[]) {
		ComputeTaxAndTotal demo = new ComputeTaxAndTotal();
		int subtotal = 11050;
		double taxrate = 5.5; 
		
		demo.TaxAndTotal(subtotal,taxrate);
		
		
	}

}/*
The subtotal = 110 dollars and 50 cents
The tax rate = 5.5 percent.
The tax = 6 dollars and 7 cents
The total = 116 dollars and 57 cents
*/